# Snifx  [![N|Solid](http://snifx.lazyweaver.com/img/snifx-logo.png)](http://snifx.lazyweaver.com/)

Snifx (**S**pring **N**ative **I**nstallable Java**FX**) is an application template for developing Desktop Java FX applications with Spring and finally creating a "fat" Jar for Windows, Linux and MacOS via Maven.

#### Features
  - Spring core and Spring context 5.1 (works with 4.1 too)
  - Java 11 or more...
  - Can package application as all-platform Jar (java -jar ...)
  - A Spring component is a JavaFX component at the same time
  - Contains some basic UI components for Demo purposes
  - Internationalization supported and added for Demo purposes
  - A default skin with some images for buttons
  - Application data stored under user home directory
  - Application "fat" Jar runnings have been tested on Windows, Linux and MacOS

# Example Component
```
@Component("exampleLayout")
public class ExampleController ... implements ApplicationListener<ApplicationEvent>, InitializingBean {

    @FXML
    private Label statusLabel;
    @FXML
    private Label demoLabel;

    @Autowired
    private AlertManager alertManager;
    @Value("${app.version}")
    private String appVersion;

    @Override
    public void afterPropertiesSet() throws Exception {
        // supports Spring initialization...    
    }

    @Override
    protected void afterFxmlInitialize() {
        // supports JavaFX initialization... 
    }

    @FXML
    private void handleAbout() {
        // opens a popup Dialog from a menu click
        // alertManager injected with Spring
        alertManager.showInfo("app.title", "app.about.header", "app.about.content", appVersion);
    }
```

### Tech

Packages:

* **src/main/java** - well, the Java code
* **src/main/java/com/lazyweaver/snifx/components/controller** - your Spring/JavaFX components. There is an example for you "RootLayoutController".
* **src/main/resources/resources/fxml** - FXML files
* **src/main/resources/resources/i18n** - internationalization files. Accommodates sample properties for English, French, German and Russian
* **src/main/resources/resources/skins** - so far has one skin "default" with main.css and a couple of icon PNGs
* **src/main/deploy** - (optional) consists of sample resources related to creating native installers (exe for Windows, deb/rpm for Linux, dmg for MacOS, etc.) via [javapackager](https://docs.oracle.com/javase/8/docs/technotes/guides/deploy/packager.html "javapackager") or third-party tools. Contains sample PNG, BMP and other images that can be replaced by you. Config files include "YourApp" and "YourCompany" values that can be totally replaced.

And of course Snifx itself is open source with a public repository on GitLab.

### Build
Building the fat jar is as simple as:
`mvn clean`
`mvn compile package`
The generated fat Jar which includes all the needed libraries and OS-specific native binaries can be found under *ProjectPath/shade/snifx.jar*

### Run
Can run by double-clicking on the generated *ProjectPath/shade/snifx.jar*
or execute
`java -jar snifx.jar`

### Native Installers
#### Native Installers (Windows) :
   * EXE creation - Install on your Windows "Inno Setup" (http://www.jrsoftware.org/isinfo.php)
   * MSI creation - Install on your Windows "WiX Toolset" (http://wixtoolset.org/). MSI packages are generated using the Windows Installer XML (WiX) toolset (also known as WiX). As of JavaFX 2.2, WiX 3.0 or later is required, and it must be available on the PATH. To validate, try running candle /? from the command line where you launch the build or from your build script.
   * Use [javapackager](https://docs.oracle.com/javase/8/docs/technotes/guides/deploy/packager.html "javapackager") documentation to create the command for your need and feel free to use any resource under *src/main/deploy/package/windows* directory

#### Native Installers (Linux) :
   * RPM installer - Install "rpmbuild" (RPM Build) 
   * DEB installer - Install "dpkg-deb"(Debian packaging tools) and "fakeroot"
   * Use [javapackager](https://docs.oracle.com/javase/8/docs/technotes/guides/deploy/packager.html "javapackager") documentation to create the command for your need and feel free to use any resource under *src/main/deploy/package/linux* directory

#### Native Installers (MacOS) :
   * No special tools are needed to generate a DMG, PKG or App - just a decently recent version of OSX.
   * Use [javapackager](https://docs.oracle.com/javase/8/docs/technotes/guides/deploy/packager.html "javapackager") documentation to create the command for your need and feel free to use any resource under *src/main/deploy/package/macos* directory

**Note:** You are free to remove the content from any of the OS deploy directories (ex. *src/main/deploy/package/windows*). Then the a javapackager execution will create new ones that can be your templates of the corresponding installer. Just observe the logs as the files will be placed under a temp directory of your OS.

### Todos

 - Add examples with javapackafer for Windows, Linux and MacOS
 - Write MORE Tests

License
----

**Free Software, open source!**