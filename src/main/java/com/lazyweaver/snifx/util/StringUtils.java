package com.lazyweaver.snifx.util;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

public final class StringUtils {

    private StringUtils() {
    }

    public static <T> String toStringSorted(List<T> items, Comparator<T> comparator, String separator, Function<T, String> toStringFunction) {
        Objects.requireNonNull(toStringFunction, "A string conversion Function implementation is mandatory");
        separator = (separator == null) ? "," : separator;
        if (items == null || items.isEmpty()) {
            return "";
        }
        Collections.sort(items, comparator);
        StringBuilder bld = new StringBuilder();
        for (int i = 0; i < items.size(); i++) {
            if (i > 0) {
                bld.append(separator);
            }
            bld.append(toStringFunction.apply(items.get(i)));
        }
        return bld.toString();
    }
}
