package com.lazyweaver.snifx.util;

import java.util.Properties;

public class PropertiesSystemOverriden extends Properties {

    private static final long serialVersionUID = 4918816302367718218L;

    @Override
    public String getProperty(String key) {
        String val = System.getProperty(key);
        if (val != null) {
            return val;
        }
        return super.getProperty(key);
    }

    @Override
    public String getProperty(String key, String defaultValue) {
        String val = getProperty(key);
        return (val == null) ? defaultValue : val;
    }
}
