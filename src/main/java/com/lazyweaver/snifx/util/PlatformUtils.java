package com.lazyweaver.snifx.util;

import com.sun.javafx.tk.Toolkit;

import javafx.application.Platform;

public final class PlatformUtils {

    private PlatformUtils() {
    }

    /**
     * Runs the code if in FX thread, otherwise wrap execute in
     * "Platform.runLater(() -> {... })"
     * 
     * @param runnable
     */
    public static <T> void runLaterIfNotFxThread(Runnable runnable) {
        if (Toolkit.getToolkit().isFxUserThread()) {
            runnable.run();
        } else {
            Platform.runLater(() -> runnable.run());
        }
    }
}
