package com.lazyweaver.snifx.util;

import java.io.File;

public final class LocalOs {

    private static final boolean isWindows = System.getProperty("os.name").startsWith("Windows");
    private static final boolean isMac = System.getProperty("os.name").startsWith("Mac");
    private static final boolean isLinux = System.getProperty("os.name").toLowerCase().contains("linux");
    private static final boolean isUnix;
    static {
        String os = System.getProperty("os.name").toLowerCase();
        isUnix = (os.indexOf("nix") >= 0 || os.indexOf("nux") >= 0 || os.indexOf("aix") > 0 || os.indexOf("sunos") > 0 || os.indexOf("bsd") > 0);
    }

    public static boolean isWindows() {
        return isWindows;
    }

    public static boolean isMac() {
        return isMac;
    }

    public static boolean isLinux() {
        return isLinux;
    }

    public static boolean isUnix() {
        return isUnix;
    }

    public static String getUserHome() {
        return System.getProperty("user.home");
    }

    public static File getUserHomeDirectory(String directoryChildName) {
        directoryChildName = (directoryChildName == null) ? "" : directoryChildName.trim();
        File userHome = new File(getUserHome());
        if (directoryChildName.isEmpty()) {
            return userHome;
        }
        return new File(userHome, directoryChildName);
    }
}
