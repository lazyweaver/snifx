package com.lazyweaver.snifx.util;

public class ActionResult {

    private static final ActionResult OK = new ActionResult(null, null, true);
    private final String errorMessage;
    private final String errorCode;
    private final boolean isSuccess;

    private ActionResult(String errorMessage, String errorCode, boolean isSuccess) {
        this.errorMessage = errorMessage;
        this.errorCode = errorCode;
        this.isSuccess = isSuccess;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public static ActionResult success() {
        return OK;
    }

    public static ActionResult fail(String message) {
        return fail(null, message);
    }

    public static ActionResult fail(String code, String message) {
        return new ActionResult(message, code, false);
    }

    public String extractWrappedErrorAndSize() {
        String searchFor = ". Error : ";
        String errMessage = errorMessage;
        int idx = errMessage.lastIndexOf(searchFor);
        if (idx > 0) {
            errMessage = errMessage.substring(idx + searchFor.length());
        }
        // strip if very big, probably contains a Throwable stacktrace
        if (errMessage.length() > 600) {
            idx = errMessage.indexOf("at ");
            if (idx > 0) {
                errMessage = errMessage.substring(0, idx).trim();
                if (errMessage.length() > 600) {
                    idx = errMessage.indexOf("Exception ");
                    if (idx > 0) {
                        errMessage = errMessage.substring(idx);
                    }
                }
            } else {
                errMessage = errMessage.substring(0, 600);
            }
            errMessage += "...";
        }
        return errMessage;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getClass().getName()).append(" [errorMessage=");
        builder.append(errorMessage);
        builder.append(", errorCode=");
        builder.append(errorCode);
        builder.append(", isSuccess=");
        builder.append(isSuccess);
        builder.append(']');
        return builder.toString();
    }
}
