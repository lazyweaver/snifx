package com.lazyweaver.snifx.util;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class RuntimeStorageConfigurator {

    private final String storageDirName;

    public RuntimeStorageConfigurator(String storageDirName) {
        this.storageDirName = storageDirName;
    }

    public File getApplicationStorageHomeDirectory() {
        return LocalOs.getUserHomeDirectory(storageDirName);
    }

    public String getApplicationStorageHomeDirectoryPath() {
        return getApplicationStorageHomeDirectory().getAbsolutePath() + File.separator;
    }

    public Map<String, String> getRuntimeMap() {
        Map<String, String> runtimeMap = new HashMap<>(1);
        runtimeMap.put("{$app.user.home}", getApplicationStorageHomeDirectoryPath().replace("\\", "/"));
        return runtimeMap;
    }
}
