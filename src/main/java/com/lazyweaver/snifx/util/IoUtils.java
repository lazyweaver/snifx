package com.lazyweaver.snifx.util;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.log4j.Logger;
import org.springframework.util.StringUtils;

public final class IoUtils {

    private static final Logger log = Logger.getLogger(IoUtils.class);

    private IoUtils() {
    }

    public static void close(Closeable... cs) {
        if (cs == null || cs.length == 0) {
            return;
        }
        for (Closeable c : cs) {
            if (c != null) {
                try {
                    c.close();
                } catch (IOException e) {
                    // Close quietly
                }
            }
        }
    }

    public static Properties load(String file, boolean isExternalResource) throws IOException {
        if (StringUtils.isEmpty(file)) {
            throw new IOException("Unable to load Properties from an empty file path");
        }
        Properties prop = new PropertiesSystemOverriden();
        InputStream input = null;
        try {
            input = getInputStream(file, isExternalResource);
            prop.load(input);
        } finally {
            close(input);
        }
        return prop;
    }

    public static InputStream getInputStream(String file, boolean isExternalResource) throws IOException {
        InputStream is = (isExternalResource) ? new FileInputStream(file) : IoUtils.class.getResourceAsStream(file);
        if (is == null) {
            throw new IOException("Empty stream from " + file);
        }
        return is;
    }

    /**
     * Will try to unzip the supplied ZIP to the supplied directory. If the
     * latter does not exist, it will be created. If the directory exists but is
     * not empty, an exception will be thrown.
     * 
     * @param ZIP
     *            file path
     * @param isZipExernalResource
     * @param destDir
     * @throws IOException
     */
    public static void unzipAll(String zipFilePath, boolean isZipExernalResource, final File destDir) throws IOException {
        ZipInputStream zis = null;
        try {
            zis = new ZipInputStream(getInputStream(zipFilePath, isZipExernalResource));
            unzipAll(zis, destDir);
        } finally {
            close(zis);
        }
    }

    /**
     * Will try to unzip to the supplied directory. If it does not exist, it
     * will be created. If the directory exists but is not empty, an exception
     * will be thrown.
     * 
     * @param ZIP
     *            inputStream
     * @param destDir
     * @throws IOException
     */
    public static void unzipAll(ZipInputStream zis, final File destDir) throws IOException {
        {
            // create output directory if it doesn't exist
            boolean isExisting = Objects.requireNonNull(destDir, "The destination directory parameter is mandatory").exists();
            if (!isExisting && !createDirectory(destDir).isSuccess()) {
                throw new IOException("Unable to create the non-existing directory " + destDir.getAbsolutePath());
            }
            if (isExisting && !destDir.isDirectory()) {
                throw new IOException("Not a directory " + destDir.getAbsolutePath());
            }
            if (isExisting && destDir.list().length > 0) {
                throw new IOException("The destination directory \"" + destDir.getAbsolutePath() + "\" is not empty");
            }
        }
        // buffer for read and write data to file
        byte[] buffer = new byte[1024];
        FileOutputStream fos = null;
        try {
            ZipEntry ze = zis.getNextEntry();
            while (ze != null) {
                final File newFile = new File(destDir, ze.getName());
                if (ze.isDirectory()) {
                    if (createDirectory(newFile).isSuccess()) {
                        log.debug("Unzipping to the new directory " + newFile.getAbsolutePath());
                    } else {
                        throw new IOException("Unable to create local directory from Zip : " + newFile.getAbsolutePath());
                    }
                } else {
                    log.debug("Unzipping file to " + newFile.getAbsolutePath());
                    // create directories for sub directories in zip
                    Files.createDirectories(newFile.getParentFile().toPath());
                    fos = new FileOutputStream(newFile);
                    int len;
                    while ((len = zis.read(buffer)) > 0) {
                        fos.write(buffer, 0, len);
                    }
                    fos.close();
                }
                // close this ZipEntry
                zis.closeEntry();
                ze = zis.getNextEntry();
            }
            // close last ZipEntry
            zis.closeEntry();
        } finally {
            close(fos);
        }
    }

    public static ActionResult createDirectory(File directory) {
        if (directory == null || directory.isFile()) {
            log.error("Unable to create directory structure as the supplied argument is null or a file :" + directory);
            return ActionResult.fail("app.invalid.argument", "the supplied argument is null or a file");
        }
        if (directory.isDirectory()) {
            log.warn("Will skip creating new directory as it already exists \"" + directory.getAbsolutePath() + "\"");
            return ActionResult.success();
        }
        try {
            Files.createDirectory(directory.toPath());
        } catch (IOException e) {
            log.error("Unable to create directory of (" + directory.getAbsolutePath() + ") due to : " + e.getMessage(), e);
            return ActionResult.fail("app.exception", e.getClass().getName() + ":" + e.getMessage());
        }
        return ActionResult.success();
    }

    public static ActionResult createDirectories(File directory) {
        if (directory == null || directory.isFile()) {
            log.error("Unable to create directory structure as the supplied argument is null or a file :" + directory);
            return ActionResult.fail("app.invalid.argument", "the supplied argument is null or a file");
        }
        try {
            Files.createDirectories(directory.toPath());
        } catch (IOException e) {
            log.error("Unable to create directory structure of (" + directory.getAbsolutePath() + ") due to : " + e.getMessage(), e);
            return ActionResult.fail("app.exception", e.getClass().getName() + ":" + e.getMessage());
        }
        log.debug("new directory \"" + directory.getAbsolutePath() + "\" created");
        return ActionResult.success();
    }

    public static boolean copyFile(String fromPath, String toPath) {
        if (fromPath == null || toPath == null) {
            return false;
        }
        return copyFile(new File(fromPath), new File(toPath));
    }

    public static boolean copyFile(File from, File to) {
        return copyFile(from, to, false);
    }

    public static boolean copyFile(File from, File to, boolean isOverwriteTarget) {
        if (from == null || !from.exists() || to == null) {
            return false;
        }
        if (isOverwriteTarget && to.exists()) {
            if (to.isFile() && !to.delete()) {
                return false;
            }
            if (to.isDirectory() && !deleteDirectory(to)) {
                return false;
            }
        }
        if (!isOverwriteTarget && to.exists()) {
            return false;
        }
        if ((from.isFile() && to.isDirectory()) || (from.isDirectory() && to.isFile())) {
            log.error("Both files should be either files or directories");
            return false;
        }
        boolean isSuccess = true;
        try {
            if (from.isFile()) {
                Files.copy(Paths.get(from.getAbsolutePath()), Paths.get(to.getAbsolutePath()));
            } else {
                if (!createDirectory(to).isSuccess()) {
                    throw new IOException("Unable to create directory " + to.getAbsolutePath());
                }
                for (File f : from.listFiles()) {
                    File toFile = new File(to, f.getName());
                    if (!copyFile(f, toFile)) {
                        throw new IOException("Unable to copy from \"" + to.getAbsolutePath() + "\" to \"" + toFile.getAbsolutePath() + "\"");
                    }
                }
            }

        } catch (IOException e) {
            isSuccess = false;
            log.error("Unable to copy from \"" + from + "\" to \"" + to + "\" due to : " + e.getMessage(), e);
        }
        return isSuccess;
    }

    public static boolean saveToFile(byte[] data, File dest) {
        if (data == null || dest == null) {
            log.error("Unable to save bytes to file - one of the parameters is null");
            return false;
        }
        boolean isSuccess = true;
        try {
            Files.write(Paths.get(dest.getAbsolutePath()), data, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            log.error("Unable to save data to " + dest.getAbsolutePath() + " due to :" + e.getMessage(), e);
            isSuccess = false;
        }
        return isSuccess;
    }

    public static boolean deleteDirectory(File dir) {
        if (!deleteDirectoryContent(dir)) {
            return false;
        }
        return dir.delete();
    }

    public static boolean deleteDirectoryContent(File dir) {
        if (dir == null || !dir.isDirectory()) {
            return false;
        }
        for (File f : dir.listFiles()) {
            if (f.isDirectory() && !deleteDirectoryContent(f)) {
                log.error("Unable to delete directory content - " + f.getAbsolutePath());
                return false;
            }
            if (!f.delete()) {
                log.error("Unable to delete " + (dir.isFile() ? "file" : "directory") + " - " + f.getAbsolutePath());
                return false;
            }
            log.debug("Deleted " + (dir.isFile() ? "file" : "directory") + " successfully - " + f.getAbsolutePath());
        }
        return true;
    }

    public static byte[] toByteArraySortedKeys(Map<?, ?> map) {
        Objects.requireNonNull(map, "Map parameter is mandatory");
        final StringBuilder out = new StringBuilder();
        List<Object> sortedKeys = new ArrayList<>(map.keySet());
        Collections.sort(sortedKeys, new Comparator<Object>() {
            @Override
            public int compare(Object o1, Object o2) {
                if (o1 == null || o2 == null) {
                    return 0;
                }
                return o1.toString().compareTo(o2.toString());
            }
        });
        for (Object key : sortedKeys) {
            out.append(key).append('=').append(map.get(key)).append(System.getProperty("line.separator"));
        }
        return out.toString().getBytes();
    }
}
