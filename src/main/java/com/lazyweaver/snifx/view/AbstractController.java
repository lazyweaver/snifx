package com.lazyweaver.snifx.view;

import java.net.URL;
import java.util.ResourceBundle;

import org.apache.log4j.Logger;

import javafx.fxml.Initializable;

public abstract class AbstractController implements Initializable {

    protected final Logger log = Logger.getLogger(getClass());
    private ResourceBundle resourceBundle = null;

    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        setResourceBundle(resourceBundle);
        afterFxmlInitialize();
    }

    public ResourceBundle getResourceBundle() {
        return resourceBundle;
    }

    public void setResourceBundle(ResourceBundle resourceBundle) {
        this.resourceBundle = resourceBundle;
    }

    public String getTitle() {
        return getResourceBundle().getString("app.title");
    }

    protected void afterFxmlInitialize() {
    }
}
