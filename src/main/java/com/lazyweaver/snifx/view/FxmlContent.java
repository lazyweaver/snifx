package com.lazyweaver.snifx.view;

public class FxmlContent<T, Y extends AbstractController> {

    private final T loadedObject;
    private final Y controller;

    public FxmlContent(T loadedObject, Y controller) {
        this.loadedObject = loadedObject;
        this.controller = controller;
    }

    public T getLoadedObject() {
        return loadedObject;
    }

    public Y getController() {
        return controller;
    }
}
