package com.lazyweaver.snifx.view;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.lazyweaver.snifx.components.locale.ResourceBundleFacade;
import com.lazyweaver.snifx.components.ui.AlertManager;
import com.lazyweaver.snifx.components.ui.SkinManager;

import javafx.fxml.FXML;

public abstract class AbtsractWindowController extends AbstractController {

    @Autowired
    protected SkinManager skinManager;
    @Autowired
    protected AlertManager alertManager;
    @Autowired
    protected ResourceBundleFacade resourceBundleFacade;
    @Value("${app.icon}")
    private String icon = null;

    public String getIcon() {
        return skinManager.getResourcesPath() + icon;
    }

    /**
     * Closes the application.
     */
    @FXML
    public void handleExit() {
        if (alertManager.showConfirm("app.confirm.exit")) {
            System.exit(0);
        }
    }
}
