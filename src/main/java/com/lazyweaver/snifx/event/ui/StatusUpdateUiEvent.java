package com.lazyweaver.snifx.event.ui;

import org.springframework.context.ApplicationEvent;

public class StatusUpdateUiEvent extends ApplicationEvent {

    private static final long serialVersionUID = -4104328205776756890L;
    private final String text;

    public StatusUpdateUiEvent(Object source, String text) {
        super(source);
        this.text = text;
    }

    public String getText() {
        return text;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getClass().getName()).append(" [text=");
        builder.append(text);
        builder.append("]");
        return builder.toString();
    }
}
