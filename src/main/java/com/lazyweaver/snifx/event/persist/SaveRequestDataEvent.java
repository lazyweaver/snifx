package com.lazyweaver.snifx.event.persist;

import java.util.Objects;

public class SaveRequestDataEvent extends PersistEvent {

    private static final long serialVersionUID = -4979199089742391672L;

    public static enum SaveAs {
        FILE
    };

    private final byte[] data;
    private final SaveAs dataTargetType;
    private final String dataTarget;

    /**
     * Construction of event for saving to File
     * 
     * @param source
     * @param data
     *            to write
     * @param dataTarget
     *            write to
     * @param entity
     */
    public SaveRequestDataEvent(Object source, byte[] data, String dataTarget, Entity entity) {
        this(source, data, dataTarget, SaveAs.FILE, entity);
    }

	/**
	 * Construction of event
	 * 
	 * @param source
	 * @param data to write
	 * @param data target
	 * @param data target type
	 * @param entity
	 */
	public SaveRequestDataEvent(Object source, byte[] data, String dataTarget, SaveAs dataTargetType, Entity entity) {
		super(source, entity);
		this.data = Objects.requireNonNull(data, "Data must NOT be null");
		this.dataTarget = Objects.requireNonNull(dataTarget, "dataTarget must NOT be null");
		this.dataTargetType = Objects.requireNonNull(dataTargetType, "dataTargetType must NOT be null");
	}

    public byte[] getData() {
        return data;
    }

    public String getDataTarget() {
        return dataTarget;
    }

    public SaveAs getDataTargetType() {
        return dataTargetType;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(getClass().getName()).append(" [dataTargetType=");
        builder.append(dataTargetType);
        builder.append(", dataTarget=");
        builder.append(dataTarget);
        builder.append(", source=");
        builder.append(source);
        builder.append("]");
        return builder.toString();
    }
}
