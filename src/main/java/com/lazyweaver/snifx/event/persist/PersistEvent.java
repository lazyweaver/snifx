package com.lazyweaver.snifx.event.persist;

import org.springframework.context.ApplicationEvent;

public abstract class PersistEvent extends ApplicationEvent {

    private static final long serialVersionUID = 8955271781149866963L;
    private final Entity entity;

    public static enum Entity {
        UserSettings
    };

    public PersistEvent(Object source, Entity entity) {
        super(source);
        this.entity = entity;
    }

    public Entity getEntity() {
        return entity;
    }
}
