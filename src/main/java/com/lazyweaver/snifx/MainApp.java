package com.lazyweaver.snifx;

import org.apache.log4j.Logger;

import com.lazyweaver.snifx.components.ComponentFacade;
import com.lazyweaver.snifx.view.AbtsractWindowController;
import com.lazyweaver.snifx.view.FxmlContent;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class MainApp extends Application {

    private static final Logger log = Logger.getLogger(MainApp.class);
    private static Stage currentStage = null;

    @Override
    public void start(Stage stage) {
        currentStage = stage;
        initStage(stage);
    }

    private static void initStage(Stage stage) {
        try {
            FxmlContent<BorderPane, AbtsractWindowController> fxmlRootLayout = ComponentFacade.loadFxml("rootLayout");
            stage.getIcons().add(new Image(fxmlRootLayout.getController().getIcon()));
            stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
                @Override
                public void handle(WindowEvent event) {
                    fxmlRootLayout.getController().handleExit();
                    event.consume();
                }
            });
            stage.setTitle(fxmlRootLayout.getController().getTitle());
            ComponentFacade.getCssStyler().setSkinStylesheet(fxmlRootLayout.getLoadedObject());
            stage.setScene(new Scene(fxmlRootLayout.getLoadedObject()));
            stage.show();

        } catch (Throwable t) {
            log.error("Error initializing stage from main : " + t.getMessage(), t);
        }
    }

    public static void restartStage() {
        initStage(currentStage);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
