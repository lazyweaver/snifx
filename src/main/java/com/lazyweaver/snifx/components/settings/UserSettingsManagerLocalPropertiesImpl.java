package com.lazyweaver.snifx.components.settings;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.lazyweaver.snifx.event.persist.PersistEvent.Entity;
import com.lazyweaver.snifx.event.persist.SaveRequestDataEvent;
import com.lazyweaver.snifx.util.ActionResult;
import com.lazyweaver.snifx.util.IoUtils;
import com.lazyweaver.snifx.util.RuntimeStorageConfigurator;

@Component("userSettingsManager")
@PropertySource(value = "classpath:resources/application.properties")
public class UserSettingsManagerLocalPropertiesImpl implements UserSettingsManager, InitializingBean {

    private Properties props;

    @Autowired
    private ApplicationEventPublisher eventPublisher;
    private Locale locale = null;

    @Value("${app.user.settings.dir.path}")
    private String fileDir;
    @Value("${app.user.settings.file}")
    private String fileName;
    @Value("${app.storage.directory}")
    private String appStorageDirectory;

    private File userPersistFile;

    @Override
    public void afterPropertiesSet() throws Exception {
        userPersistFile = new File(getDynamic(fileDir), fileName);
        createPersistFileIfNotExisting();
        props = IoUtils.load(userPersistFile.getAbsolutePath(), true);
        String localeVal = get("user.locale");
        locale = localeVal != null ? new Locale(localeVal) : new Locale("en");
    }

    @Override
    public Locale getLocale() {
        return locale;
    }

    @Override
    public void setLocale(Locale locale) {
        if (!this.locale.equals(locale)) {
            this.locale = locale;
            set("user.locale", locale.getLanguage());
        }
    }

    @Override
    public String get(String key, Object... attributes) {
        if (!isSet(key)) {
            throw new IllegalArgumentException("No such key \"" + key + "\" in settings \"" + userPersistFile.getAbsolutePath() + "\"");
        }
        String value = props.getProperty(key);
        if (attributes != null && attributes.length > 0) {
            value = MessageFormat.format(value, attributes);
        }
        return value;
    }

    @Override
    public void set(String key, String value) {
        props.setProperty(key, value);
        eventPublisher.publishEvent(new SaveRequestDataEvent(this, IoUtils.toByteArraySortedKeys(props), userPersistFile.getAbsolutePath(), Entity.UserSettings));
    }

    @Override
    public boolean isSet(String key) {
        return props.containsKey(key);
    }

    private void createPersistFileIfNotExisting() throws IOException {
        if (userPersistFile.isDirectory()) {
            throw new IllegalStateException(userPersistFile.getAbsolutePath() + " is a directory! Expecting a file.");
        }
        if (!userPersistFile.isFile()) {
            if (!userPersistFile.getParentFile().isDirectory()) {
                ActionResult result = IoUtils.createDirectories(userPersistFile.getParentFile());
                if (!result.isSuccess()) {
                    throw new IOException("Unable to create directory structure \"" + userPersistFile.getParentFile().getAbsolutePath() + "\" due to : " + result.getErrorMessage());
                }
            }
            createNewSetupOnConstruct(userPersistFile);
        }
    }

    private String getDynamic(String value, Object... attributes) {
        Map<String, String> runtimeMap = new RuntimeStorageConfigurator(appStorageDirectory).getRuntimeMap();
        for (String mapKey : runtimeMap.keySet()) {
            value = value.replace(mapKey, runtimeMap.get(mapKey));
        }
        return value;
    }

    private void createNewSetupOnConstruct(File settingsFile) throws IOException {
        try {
            IoUtils.unzipAll("/resources/setup/user-settings-new.zip", false, settingsFile.getParentFile());
        } catch (IOException e) {
            throw new RuntimeException("Unable to extract \"user-settings-new.zip\" content to \"" + settingsFile.getParentFile().getAbsolutePath() + "\" due to : " + e.getMessage());
        }
    }
}
