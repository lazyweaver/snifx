package com.lazyweaver.snifx.components.settings;

import java.util.Locale;

public interface UserSettingsManager {

    public boolean isSet(String key);
    public String get(String key, Object ... attributes);
    public void set(String key, String value);
    public Locale getLocale();
    public void setLocale(Locale locale);
}
