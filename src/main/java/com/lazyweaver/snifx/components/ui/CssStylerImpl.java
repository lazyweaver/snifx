package com.lazyweaver.snifx.components.ui;

import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javafx.css.Styleable;
import javafx.scene.Parent;
import javafx.scene.web.WebEngine;

@Component("cssStyler")
@PropertySource(value = "classpath:resources/skin.properties")
public class CssStylerImpl implements CssStyler {

    private static final Logger log = Logger.getLogger(CssStylerImpl.class);

    @Autowired
    private SkinManager skinManager;
    @Value("${app.skin.stylesheet}")
    private String stylesheet;

    @Override
    public void setSingleStyleClass(Styleable styleable, String className) {
        setStyleClass(styleable, className, true, true);
    }

    @Override
    public void addStyleClass(Styleable styleable, String className) {
        addStyleClass(styleable, className, false);
    }

    @Override
    public void addStyleClass(Styleable styleable, String className, boolean isClearUserDefinedOnly) {
        setStyleClass(styleable, className, false, isClearUserDefinedOnly);
    }

    @Override
    public void setSkinStylesheet(Parent parent) {
        if (parent == null) {
            log.error("Unable to set the CSS stylesheet due to an empty param [NULL]");
            return;
        }
        parent.getStylesheets().clear();
        parent.getStylesheets().add(skinManager.getResourcesPath() + stylesheet);
    }

    @Override
    public void setSkinStylesheet(WebEngine webEngine) {
        if (webEngine == null) {
            log.error("Unable to set the CSS stylesheet due to an empty param [NULL]");
            return;
        }
        try {
            webEngine.setUserStyleSheetLocation(Thread.currentThread().getContextClassLoader().getResource(skinManager.getResourcesPath() + stylesheet).toString());
        } catch (Exception e) {
            log.error("Unable to set CSS file to webEngine due to : " + e.getMessage(), e);
        }
    }

    private static void setStyleClass(Styleable styleable, String className, boolean isClearAll, boolean isClearUserDefinedOnly) {
        if (styleable == null || StringUtils.isEmpty(className)) {
            log.error("Unable to set the CSS class due to at least one empty param [" + styleable + ", " + className + "]");
            return;
        }
        if (isClearAll) {
            styleable.getStyleClass().clear();
        }
        if (isClearUserDefinedOnly) {
            styleable.getStyleClass().removeAll(userUsedCssClasses);
        }
        userUsedCssClasses.add(className);
        styleable.getStyleClass().add(className);
    }

    private static final Set<String> userUsedCssClasses = new HashSet<>();
}
