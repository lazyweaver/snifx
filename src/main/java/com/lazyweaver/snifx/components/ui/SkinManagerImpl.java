package com.lazyweaver.snifx.components.ui;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import com.lazyweaver.snifx.components.settings.UserSettingsManager;

@Component("skinManager")
@PropertySource(value = "classpath:resources/skin.properties")
public class SkinManagerImpl implements SkinManager, InitializingBean {

    private String skinName = null;

    @Autowired
    private UserSettingsManager userSettingsManager;
    @Value("${app.skin.path}")
    private String rootPath;
    @Value("${app.skin.mask}")
    private String rootPathSkinMask;
    @Value("${app.skin.user.key}")
    private String userDefinedSkinKey;
    @Value("${app.skin.default}")
    private String defaultSkin;

    @Override
    public void afterPropertiesSet() throws Exception {
        this.skinName = userSettingsManager.isSet(userDefinedSkinKey) ? userSettingsManager.get(userDefinedSkinKey) : defaultSkin;
    }

    @Override
    public String getResourcesPath() {
        return rootPath.replace(rootPathSkinMask, skinName);
    }

    @Override
    public String getSkinName() {
        return skinName;
    }
}
