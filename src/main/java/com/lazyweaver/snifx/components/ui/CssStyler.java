package com.lazyweaver.snifx.components.ui;

import javafx.css.Styleable;
import javafx.scene.Parent;
import javafx.scene.web.WebEngine;

public interface CssStyler {

    public void setSingleStyleClass(Styleable styleable, String className);
    public void addStyleClass(Styleable styleable, String className);
    public void addStyleClass(Styleable styleable, String className, boolean isClearUserDefinedOnly);
    public void setSkinStylesheet(Parent parent);
    public void setSkinStylesheet(WebEngine webEngine);
}
