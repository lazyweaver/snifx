package com.lazyweaver.snifx.components.ui;

import static com.lazyweaver.snifx.util.PlatformUtils.runLaterIfNotFxThread;

import java.text.MessageFormat;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.lazyweaver.snifx.components.locale.ResourceBundleFacade;

import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceDialog;
import javafx.scene.control.Dialog;
import javafx.scene.control.Labeled;
import javafx.scene.control.TextInputDialog;

@Component("alertManager")
public class AlertManagerImpl implements AlertManager {

    @Autowired
    private CssStyler cssStyler;
    @Autowired
    private WidgetManager widgetManager;
    @Autowired
    private ResourceBundleFacade resourceBundleFacade;

    @Override
    public void showInfo(String contentKey, Object... arguments) {
        showInfo("app.title", "app.notification.header", contentKey, arguments);
    }

    @Override
    public void showInfo(String titleKey, String headerKey, String contentKey, Object... arguments) {
        showInfoAsText(titleKey, headerKey, contentKey, arguments);
    }

    @Override
    public boolean showConfirm(String contentKey, Object... arguments) {
        return showConfirm("app.confirm.title", contentKey, arguments);
    }

    @Override
    public boolean showConfirm(String titleKey, String contentKey, Object... arguments) {
        ResourceBundle resourceBundle = resourceBundleFacade.get();
        Alert alert = new Alert(AlertType.CONFIRMATION, "", new ButtonType(resourceBundle.getString("app.confirm.yes"), ButtonData.YES),
                new ButtonType(resourceBundle.getString("app.confirm.no"), ButtonData.NO));
        String content = resourceBundle.getString(contentKey);
        if (arguments != null && arguments.length > 0) {
            content = MessageFormat.format(content, arguments);
        }
        ButtonType bt = showAlert(alert, resourceBundle.getString(titleKey), content, null, true);
        return bt.getButtonData() == ButtonData.YES;
    }

    @Override
    public void showError(String contentKey, Object... arguments) {
        ResourceBundle resourceBundle = resourceBundleFacade.get();
        String errorContent = resourceBundle.getString(contentKey);
        if (arguments != null && arguments.length > 0) {
            errorContent = MessageFormat.format(errorContent, arguments);
        }
        showErrorAlert(resourceBundle, errorContent);
    }

    @Override
    public void showThrowable(Throwable throwable) {
        ResourceBundle resourceBundle = resourceBundleFacade.get();
        showErrorAlert(resourceBundle, MessageFormat.format(resourceBundle.getString("app.error.content"), throwable.getMessage()));
    }

    @Override
    public String showInputText(String contentKey, String defaultValue) {
        defaultValue = (defaultValue == null) ? "" : defaultValue;
        ResourceBundle resourceBundle = resourceBundleFacade.get();
        Dialog<String> dialog = new TextInputDialog(defaultValue);
        return showAlert(dialog, resourceBundle.getString("app.input.expected"), resourceBundle.getString(contentKey), "", true);
    }

    @Override
    public String showChoiceSelect(String contentKey, String... selectValues) {
        if (selectValues.length < 1) {
            return null;
        }
        ResourceBundle resourceBundle = resourceBundleFacade.get();
        Dialog<String> dialog = new ChoiceDialog<>(selectValues[0], selectValues);
        return showAlert(dialog, resourceBundle.getString("app.select.from"), resourceBundle.getString(contentKey), resourceBundle.getString("app.select.options.label"), true);
    }

    private void showInfoAsText(String titleKey, String headerKey, String contentKey, Object... arguments) {
        ResourceBundle resourceBundle = resourceBundleFacade.get();
        String content = resourceBundle.getString(contentKey);
        if (arguments != null && arguments.length > 0) {
            content = MessageFormat.format(content, arguments);
        }
        final String innerContent = content;
        runLaterIfNotFxThread(() -> showAlert(new Alert(AlertType.INFORMATION), resourceBundle.getString(titleKey), resourceBundle.getString(headerKey), innerContent, true));
    }

    private void showErrorAlert(ResourceBundle resourceBundle, String errorContent) {
        runLaterIfNotFxThread(() -> {
            Alert alert = new Alert(AlertType.ERROR, "", new ButtonType(resourceBundle.getString("app.confirm.close"), ButtonData.CANCEL_CLOSE));
            showAlert(alert, resourceBundle.getString("app.error.title"), errorContent, null, true);
        });
    }

    private <T> T showAlert(Dialog<T> dialog, String title, String header, String text, boolean isWait) {
        dialog.getDialogPane().getButtonTypes().forEach(bt -> applyButtonStyling(bt.getButtonData(), dialog.getDialogPane().lookupButton(bt)));
        cssStyler.setSkinStylesheet(dialog.getDialogPane());
        dialog.setTitle(title);
        dialog.setHeaderText(header);
        if (text != null && text.length() > 3000) {
            text = text.substring(0, 3000) + "...more...";
        }
        dialog.setContentText(text);
        if (isWait) {
            runLaterIfNotFxThread(() -> dialog.showAndWait());
        } else {
            runLaterIfNotFxThread(() -> dialog.show());
        }
        return dialog.getResult();
    }

    private void applyButtonStyling(ButtonData buttonData, Node button) {
        switch (buttonData) {
            case CANCEL_CLOSE:
                applyButtonStyling(button, "cancel.png");
                break;
            case NO:
                applyButtonStyling(button, "no.png");
                break;
            case YES:
                applyButtonStyling(button, "yes.png");
                break;
            case OK_DONE:
                applyButtonStyling(button, "ok.png");
                break;
            default:
                break;
        }
    }

    private void applyButtonStyling(Node button, String imageName) {
        if (button != null && button instanceof Labeled) {
            widgetManager.setGraphic((Labeled) button, imageName);
        }
    }
}
