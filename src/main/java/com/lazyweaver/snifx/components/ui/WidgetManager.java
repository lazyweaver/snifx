package com.lazyweaver.snifx.components.ui;

import javafx.scene.control.Labeled;
import javafx.scene.control.MenuBar;
import javafx.scene.image.ImageView;

public interface WidgetManager {

    public ImageView getImageView(String imageName);
    public void setGraphic(MenuBar menuBar);
    public void setGraphic(Labeled labeled, String imageName);
}
