package com.lazyweaver.snifx.components.ui;

public interface AlertManager {

    public boolean showConfirm(String contentKey, Object ... arguments);
    public boolean showConfirm(String titleKey, String contentKey, Object ... arguments);
    public void showError(String contentKey, Object ... arguments);
    public void showInfo(String contentKey, Object ... arguments);
    public void showInfo(String titleKey, String headerKey, String contentKey, Object ... arguments);
    public void showThrowable(Throwable throwable);
    public String showInputText(String contentKey, String defaultValue);
    public String showChoiceSelect(String contentKey, String ... selectValues);
}
