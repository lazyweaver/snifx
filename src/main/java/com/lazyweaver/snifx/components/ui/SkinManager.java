package com.lazyweaver.snifx.components.ui;

public interface SkinManager {

    public String getResourcesPath();
    public String getSkinName();
}
