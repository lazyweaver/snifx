package com.lazyweaver.snifx.components.ui;

import static com.lazyweaver.snifx.util.PlatformUtils.runLaterIfNotFxThread;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javafx.scene.control.Labeled;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

@Component("widgetManager")
public class WidgetManagerImpl implements WidgetManager {

    @Autowired
    private SkinManager skinManager;

    @Override
    public ImageView getImageView(final String imageName) {
        return new ImageView(new Image(skinManager.getResourcesPath() + "images/" + imageName));
    }

    @Override
    public void setGraphic(final Labeled labeled, final String imageName) {
        Objects.requireNonNull(labeled, "The labeled object is missing");
        runLaterIfNotFxThread(() -> {
            labeled.setGraphic(getImageView(imageName));
        });
    }

    @Override
    public void setGraphic(MenuBar menuBar) {
        menuBar.getMenus().stream().forEach(m -> setGraphicToMenu(m));
    }

    private void setGraphicToMenu(Menu menu) {
        if (menu.getId() != null) {
            setGraphicIfEligible(menu);
        }
        for (MenuItem menuItem : menu.getItems()) {
            setGraphicIfEligible(menuItem);
            if (menuItem instanceof Menu) {
                setGraphicToMenu((Menu) menuItem);
            }
        }
    }

    private void setGraphicIfEligible(MenuItem menuItem) {
        if (menuItem.getId() == null) {
            return;
        }
        String imageName = menuItem.getId();
        menuItem.setGraphic(getImageView(imageName));
    }
}
