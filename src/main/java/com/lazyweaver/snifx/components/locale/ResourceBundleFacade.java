package com.lazyweaver.snifx.components.locale;

import java.util.ResourceBundle;

public interface ResourceBundleFacade {

    public ResourceBundle get();
}
