package com.lazyweaver.snifx.components.locale;

import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.Vector;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.lazyweaver.snifx.components.settings.UserSettingsManager;
import com.lazyweaver.snifx.util.IoUtils;

@Component("resourceBundleFacade")
public class ResourceBundleFacadeCacheMapImpl implements ResourceBundleFacade {

    private static final Map<String, ResourceBundle> cachedMap = new HashMap<>();
    private static final String baseName = "resources/i18n/messages";
    @Autowired
    private UserSettingsManager userSettingsManager;

    @Override
    public ResourceBundle get() {
        String realBaseName = getRealBaseName(baseName, userSettingsManager.getLocale());
        ResourceBundle cached = cachedMap.get(realBaseName);
        if (cached == null) {
            cached = new ResourceBundlePropertiesImpl(realBaseName, userSettingsManager.getLocale());
            cachedMap.put(realBaseName, cached);
        }
        return cached;
    }

    private static class ResourceBundlePropertiesImpl extends ResourceBundle {
        private final Map<String, Object> lookup;

        /**
         * Construction
         * 
         * @param baseName
         * @param locale
         * @throws IOException
         */
        private ResourceBundlePropertiesImpl(String realBaseName, Locale locale) {
            lookup = new HashMap<>();
            Properties props = null;
            try {
                props = IoUtils.load(realBaseName, false);
            } catch (Exception e) {
                throw new RuntimeException("Unable to load properties file \"" + realBaseName + "\" due to :" + e.getMessage());
            }
            for (Object key : props.keySet()) {
                lookup.put(key.toString(), props.getProperty(key.toString()));
            }
        }

        @Override
        public Object handleGetObject(String key) {
            if (key == null) {
                throw new IllegalArgumentException("Mandatory key is null");
            }
            return lookup.get(key);
        }

        /**
         * Returns an <code>Enumeration</code> of the keys contained in this
         * <code>ResourceBundle</code> and its parent bundles.
         *
         * @return an <code>Enumeration</code> of the keys contained in this
         *         <code>ResourceBundle</code> and its parent bundles.
         * @see #keySet()
         */
        @Override
        public Enumeration<String> getKeys() {
            return new Vector<String>(lookup.keySet()).elements();
        }
    }

    private static String getRealBaseName(String baseName, Locale locale) {
        if (!baseName.startsWith("/")) {
            baseName = "/" + baseName;
        }
        baseName = baseName + "_" + locale.getLanguage();
        if (!baseName.endsWith(".properties")) {
            baseName += ".properties";
        }
        return baseName;
    }
}
