package com.lazyweaver.snifx.components.controller;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.lazyweaver.snifx.MainApp;
import com.lazyweaver.snifx.components.settings.UserSettingsManager;
import com.lazyweaver.snifx.event.ui.StatusUpdateUiEvent;
import com.lazyweaver.snifx.view.AbtsractWindowController;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;

@Component("rootLayout")
public class RootLayoutController extends AbtsractWindowController implements ApplicationListener<ApplicationEvent> {

    @FXML
    private Label statusLabel;
    @FXML
    private Label demoLabel;

    @Autowired
    private UserSettingsManager userSettingsManager;
    @Autowired 
    private ApplicationEventPublisher eventPublisher;
    
    @Value("${app.version}")
    private String appVersion;

    @Override
    protected void afterFxmlInitialize() {
        demoLabel.setText(resourceBundleFacade.get().getString("hello.world"));
    }

    /**
     * Opens an about dialog.
     */
    @FXML
    private void handleAbout() {
        alertManager.showInfo("app.title", "app.about.header", "app.about.content", appVersion);
    }

    /**
     * Opens an about dialog.
     */
    @FXML
    private void handleChangeLang(ActionEvent event) {
        Locale locale = new Locale(getUserMenuItemData(event));
        log.debug("Changed language \"" + locale + "\"");
        userSettingsManager.setLocale(locale);
        MainApp.restartStage();
        // Publish a Spring event example
        eventPublisher.publishEvent(new StatusUpdateUiEvent(this, userSettingsManager.getLocale().toString()));
    }

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        // Spring event listener
        if (event instanceof StatusUpdateUiEvent) {
            Platform.runLater(() -> setStatusLabelText(((StatusUpdateUiEvent) event).getText()));
            return;
        }
    }

    private void setStatusLabelText(String text) {
        if (statusLabel != null) {
            statusLabel.setText(text);
        }
    }

    private String getUserMenuItemData(ActionEvent event) {
        if (!(event.getSource() instanceof MenuItem)) {
            log.warn("Trying to get userData from a NOT MenuItem source");
            return "";
        }
        Object userparam = ((MenuItem) event.getSource()).getUserData();
        return userparam == null ? "" : userparam.toString();
    }
}