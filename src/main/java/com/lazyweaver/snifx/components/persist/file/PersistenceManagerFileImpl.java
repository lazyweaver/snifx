package com.lazyweaver.snifx.components.persist.file;

import java.io.File;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import com.lazyweaver.snifx.components.persist.PersistenceManager;
import com.lazyweaver.snifx.event.persist.SaveRequestDataEvent;
import com.lazyweaver.snifx.util.IoUtils;

@Component
public class PersistenceManagerFileImpl implements PersistenceManager, ApplicationListener<ApplicationEvent> {

    private static final Logger log = Logger.getLogger(PersistenceManagerFileImpl.class);

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (event instanceof SaveRequestDataEvent) {
            SaveRequestDataEvent saveEvent = (SaveRequestDataEvent) event;
            log.debug("Event for saving data from :" + saveEvent.getSource().getClass().getSimpleName());
            final File eventTargetFile = new File(saveEvent.getDataTarget());
            // Try to create a backup file
            if (!createBackupFile(eventTargetFile)) {
                log.error("Backup persistence error for " + saveEvent.getEntity());
                return;
            }
            if (!IoUtils.saveToFile(saveEvent.getData(), eventTargetFile)) {
                log.error("Unable to save supplied data.");
                return;
            }
            return;
        }
    }

    private static boolean createBackupFile(File eventTargetFile) {
        if (new FileBackup(eventTargetFile).create()) {
            log.debug("A backup file was created");
            return true;
        }
        log.error("A backup file was NOT created");
        return false;
    }
}
