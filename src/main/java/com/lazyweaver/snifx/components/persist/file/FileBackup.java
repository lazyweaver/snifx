package com.lazyweaver.snifx.components.persist.file;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.lazyweaver.snifx.util.IoUtils;

public class FileBackup {

    private static final int MAX_NUM = 3;
    private static final String BACKUP_SUFFIX = ".bak";
    private final File fileToRestore;
    private final List<File> backupFiles;

    public FileBackup(String fileToRestore) {
        this(new File(fileToRestore));
    }

    public FileBackup(File fileToRestore) {
        if (fileToRestore == null || !fileToRestore.isFile()) {
            throw new IllegalArgumentException("Backup initialization failed due to a missing file:" + fileToRestore);
        }
        this.fileToRestore = fileToRestore;
        backupFiles = new ArrayList<>(MAX_NUM);
        initBackupList();
    }

    public boolean create() {
        String newBackupFileName = fileToRestore.getName() + BACKUP_SUFFIX + Integer.toString(getBackupCurrentIndex() + 1);
        File newBackupFile = new File(fileToRestore.getParentFile(), newBackupFileName);
        boolean isSuccess = IoUtils.copyFile(fileToRestore, newBackupFile);
        if (isSuccess) {
            backupFiles.add(newBackupFile);
            removeOlderBackups();
        }
        return isSuccess;
    }

    public boolean restore() {
        if (backupFiles.isEmpty()) {
            return false;
        }
        return IoUtils.copyFile(getLastBackupFile(), fileToRestore, true);
    }

    public List<File> getBackupFiles() {
        return Collections.unmodifiableList(backupFiles);
    }

    public static String backupSuffix() {
        return BACKUP_SUFFIX;
    }

    private void initBackupList() {
        backupFiles.clear();
        backupFiles.addAll(Arrays.asList(fileToRestore.getParentFile().listFiles(pathname -> pathname.getName().startsWith(fileToRestore.getName()) && pathname.getName().contains(BACKUP_SUFFIX))));
        Collections.sort(backupFiles, (o1, o2) -> Integer.valueOf(getBackupIndexFromName(o1)).compareTo(Integer.valueOf(getBackupIndexFromName(o2))));
    }

    private File getLastBackupFile() {
        return backupFiles.get(backupFiles.size() - 1);
    }

    private int getBackupCurrentIndex() {
        if (backupFiles.isEmpty()) {
            return 0;
        }
        return getBackupIndexFromName(getLastBackupFile());
    }

    private static int getBackupIndexFromName(File backupFile) {
        int idx1 = backupFile.getName().lastIndexOf(BACKUP_SUFFIX) + BACKUP_SUFFIX.length();
        return Integer.parseInt(backupFile.getName().substring(idx1));
    }

    private void removeOlderBackups() {
        if (backupFiles.size() <= MAX_NUM) {
            return;
        }
        for (int i = 0; i < (backupFiles.size() - MAX_NUM); i++) {
            if (!backupFiles.get(i).delete()) {
                backupFiles.get(i).deleteOnExit();
            }
            backupFiles.remove(i);
        }
    }
}
