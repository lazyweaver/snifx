package com.lazyweaver.snifx.components;

import java.io.IOException;
import java.io.InputStream;
import java.util.ResourceBundle;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

import com.lazyweaver.snifx.components.locale.ResourceBundleFacade;
import com.lazyweaver.snifx.components.ui.CssStyler;
import com.lazyweaver.snifx.util.IoUtils;
import com.lazyweaver.snifx.view.AbstractController;
import com.lazyweaver.snifx.view.FxmlContent;

import javafx.fxml.FXMLLoader;
import javafx.util.Callback;

@ComponentScan(basePackages = "com.lazyweaver.snifx.components")
public final class ComponentFacade {

    private static AnnotationConfigApplicationContext applicationContext;
    static {
        applicationContext = new AnnotationConfigApplicationContext();
        applicationContext.register(ComponentFacade.class);
        applicationContext.refresh();
    }

    private ComponentFacade() {
    }

    public static CssStyler getCssStyler() {
        return getBean("cssStyler");
    }

    /**
     * Loads a Spring managed and JavaFX managed component
     * 
     * @param ui
     *            type
     * 
     * @return component
     * @throws IOException
     */
    public static <T, Y extends AbstractController> FxmlContent<T, Y> loadFxml(String uiLayout) {
        FXMLLoader fxmlLoader = new FXMLLoader();
        fxmlLoader.setResources(getResourceBundle());
        fxmlLoader.setControllerFactory(new Callback<Class<?>, Object>() {
            @Override
            public Object call(Class<?> clazz) {
                return getBean(uiLayout);
            }
        });
        T loaded = null;
        InputStream fxml = null;
        try {
            fxml = getFxmlInputStream(uiLayout);
            loaded = fxmlLoader.load(fxml);
        } catch (Exception e) {
            throw new RuntimeException("Unable to load FXML \"" + uiLayout + "\" due to : " + e.getMessage(), e);
        } finally {
            IoUtils.close(fxml);
        }
        return new FxmlContent<T, Y>(loaded, fxmlLoader.getController());
    }

    private static InputStream getFxmlInputStream(String uiLayout) throws IOException {
        final String fxmlPath = "/resources/fxml/" + uiLayout + ".fxml";
        InputStream fxml = FxmlContent.class.getResourceAsStream(fxmlPath);
        if (fxml == null) {
            throw new IOException("FXML resource not found under path " + fxmlPath);
        }
        return fxml;
    }

    private static <T> T getBean(String beanName) {
        @SuppressWarnings("unchecked")
        T bean = (T) applicationContext.getBean(beanName);
        return bean;
    }

    private static ResourceBundle getResourceBundle() {
        ResourceBundleFacade rbf = getBean("resourceBundleFacade");
        return rbf.get();
    }
}
