Summary: YourApp
Name: yourapp
Version: 1.0
Release: 1
License: free
Vendor: YourCompany.com
Prefix: /opt
Provides: yourapp
Requires: ld-linux.so.2 libX11.so.6 libXext.so.6 libXi.so.6 libXrender.so.1 libXtst.so.6 libasound.so.2 libc.so.6 libdl.so.2 libgcc_s.so.1 libm.so.6 libpthread.so.0 libthread_db.so.1
Autoprov: 0
Autoreq: 0

#avoid ARCH subfolder
%define _rpmfilename %%{NAME}-%%{VERSION}-%%{RELEASE}.%%{ARCH}.rpm

#comment line below to enable effective jar compression
#it could easily get your package size from 40 to 15Mb but 
#build time will substantially increase and it may require unpack200/system java to install
%define __jar_repack %{nil}

%description
YourApp description

%prep

%build

%install
rm -rf %{buildroot}
mkdir -p %{buildroot}/opt
cp -r %{_sourcedir}/YourApp %{buildroot}/opt

%files

/opt/YourApp

%post


xdg-desktop-menu install --novendor /opt/YourApp/YourApp.desktop

if [ "false" = "true" ]; then
    cp /opt/YourApp/yourapp.init /etc/init.d/yourapp
    if [ -x "/etc/init.d/yourapp" ]; then
        /sbin/chkconfig --add yourapp
        if [ "false" = "true" ]; then
            /etc/init.d/yourapp start
        fi
    fi
fi

%preun

xdg-desktop-menu uninstall --novendor /opt/YourApp/YourApp.desktop

if [ "false" = "true" ]; then
    if [ -x "/etc/init.d/yourapp" ]; then
        if [ "true" = "true" ]; then
            /etc/init.d/yourapp stop
        fi
        /sbin/chkconfig --del yourapp
        rm -f /etc/init.d/yourapp
    fi
fi

%clean
