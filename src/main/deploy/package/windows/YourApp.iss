;This file will be executed next to the application bundle image
;I.e. current directory will contain folder YourApp with application files

#define MyAppName "YourApp"
#define MyAppVersion "1.0"
#define MyAppProjectPath "C:\Dev\workspace\snifx\src\main\deploy\package\windows"
#define MyAppPublisher "YourCompanyName"
#define MyAppURL "http://your.company.com/"
#define MyAppStorageDirName "snifx-storage"

[Setup]
AppId={{com.company.yours}}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
;AppVerName={#MyAppName} {#MyAppVersion}
AppPublisher={#MyAppPublisher}
AppComments=YourApp
AppCopyright=Copyright (C) 2019
AppPublisherURL={#MyAppURL}
AppSupportURL={#MyAppURL}/support/
AppUpdatesURL={#MyAppURL}/downloads/
DefaultDirName={pf}\YourCompany\YourApp
;DisableStartupPrompt=Yes
;DisableDirPage=Yes
;DisableProgramGroupPage=Yes
;DisableReadyPage=Yes
;DisableFinishedPage=Yes
;DisableWelcomePage=Yes
DefaultGroupName=YourCompany\YourApp
;Optional License
LicenseFile=
;WinXP or above
MinVersion=0,5.1 
OutputBaseFilename={#MyAppName}-{#MyAppVersion}
Compression=lzma
SolidCompression=yes
PrivilegesRequired=lowest
UninstallDisplayIcon={app}\YourApp.ico
UninstallDisplayName={#MyAppName}-{#MyAppVersion}
SetupIconFile={#MyAppProjectPath}\YourApp.ico
WizardImageStretch=No
WizardImageFile={#MyAppProjectPath}\wizard-icon.bmp
WizardSmallImageFile={#MyAppProjectPath}\wizard-small-icon.bmp
ArchitecturesInstallIn64BitMode=x64


[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "YourApp\YourApp.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "YourApp\*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs

[Icons]
Name: "{group}\YourApp"; Filename: "{app}\YourApp.exe"; IconFilename: "{app}\YourApp.ico"; Check: returnTrue()
;Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\YourApp.exe"; IconFilename: "{app}\YourApp.ico"; Check: returnFalse()
Name: "{commondesktop}\{#MyAppName}"; Filename: "{app}\YourApp.exe"; Tasks: desktopicon

[Run]
Filename: "{app}\YourApp.exe"; Parameters: "-Xappcds:generatecache"; Check: returnFalse()
Filename: "{app}\YourApp.exe"; Description: "{cm:LaunchProgram,YourApp}"; Flags: nowait postinstall skipifsilent; Check: returnTrue()
Filename: "{app}\YourApp.exe"; Parameters: "-install -svcName ""YourApp"" -svcDesc ""YourApp"" -mainExe ""YourApp.exe""  "; Check: returnFalse()

[UninstallRun]
Filename: "{app}\YourApp.exe "; Parameters: "-uninstall -svcName YourApp -stopOnUninstall"; Check: returnFalse()

[UninstallDelete]
Type: filesandordirs; Name: "{%USERPROFILE}\{#MyAppStorageDirName}"

[Code]
function returnTrue(): Boolean;
begin
  Result := True;
end;

function returnFalse(): Boolean;
begin
  Result := False;
end;

function InitializeSetup(): Boolean;
begin
// Possible future improvements:
//   if version less or same => just launch app
//   if upgrade => check if same app is running and wait for it to exit
//   Add pack200/unpack200 support? 
  Result := True;
end;  
