package com.lazyweaver.snifx.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

public class ResourceBundleIntegrityTest {

    private static final Locale[] supportedLocales = { new Locale("en"), new Locale("de"), new Locale("fr"), new Locale("ru") };

    @Test
    public void integrationTest() {
        List<ResourceBundle> bundles = new ArrayList<>(supportedLocales.length);
        Arrays.asList(supportedLocales).stream().forEach(l -> bundles.add(ResourceBundle.getBundle("resources/i18n/messages", l)));
        final Set<String> allKeys = new HashSet<>();
        bundles.stream().forEach(rb -> allKeys.addAll(rb.keySet()));
        System.out.println("Number of i18n keys - " + allKeys.size());

        bundles.stream().forEach(rb -> allKeys.stream().forEach(key -> {
            try {
                rb.getString(key);
            } catch (MissingResourceException e) {
                Assert.fail("Missing key \"" + key + "\" in bundle \"" + rb.getBaseBundleName() + "\" with locale \"" + rb.getLocale() + "\"");
            }
        }));
    }
}
