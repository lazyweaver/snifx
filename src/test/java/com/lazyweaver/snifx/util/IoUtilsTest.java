package com.lazyweaver.snifx.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Properties;
import java.util.zip.ZipInputStream;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

public class IoUtilsTest {

    private static final String copyTestFromPath = "src/test/resources/com/ioCopyTest/test.properties";

    @BeforeClass
    public static void initAll() {
        deleteFileIfExistsFailOnError(getCopyTestBackupFilePath(), true);
    }

    @AfterClass
    public static void cleanAll() {
        deleteFileIfExistsFailOnError(getCopyTestBackupFilePath(), false);
    }

    @Test
    public void loadProperties() throws IOException {
        Properties props = IoUtils.load("src/test//resources/com/settingsTest/user.properties", true);
        Assert.assertNotNull(props);
        Assert.assertNotNull(props.getProperty("user.locale"));
        Assert.assertTrue(props.getProperty("user.locale").length() > 1);
        props = IoUtils.load("/com/test.properties", false);
        Assert.assertNotNull(props);
        Assert.assertNotNull(props.getProperty("key1"));
        Assert.assertNotNull(props.getProperty("key2"));
        Assert.assertEquals("key1value", props.getProperty("key1"));
        Assert.assertEquals("key2value", props.getProperty("key2"));
    }

    @Test
    public void toByteArraySortedKeys() {
        try {
            IoUtils.toByteArraySortedKeys(null);
            Assert.fail("Exception expected");
        } catch (NullPointerException npe) {
            Assert.assertEquals("Map parameter is mandatory", npe.getMessage());
        }
        byte[] result = null;
        Properties props = new Properties();
        //
        result = IoUtils.toByteArraySortedKeys(props);
        Assert.assertNotNull(result);
        Assert.assertEquals("", new String(result));
        //
        props.put("cda", "cdaVal");
        props.put("abc", "abcVal");
        props.put("ggg", "gggVal");
        props.put("ppp", "pppVal");
        props.put("bca", "bcaVal");
        result = IoUtils.toByteArraySortedKeys(props);
        Assert.assertNotNull(result);
        String NL = System.getProperty("line.separator");
        Assert.assertEquals("abc=abcVal" + NL + "bca=bcaVal" + NL + "cda=cdaVal" + NL + "ggg=gggVal" + NL + "ppp=pppVal" + NL, new String(result));
    }

    @Test
    public void copyFileAsFiles() {
        Assert.assertTrue(new File(copyTestFromPath).isFile());
        Assert.assertFalse(IoUtils.copyFile((String) null, (String) null));
        Assert.assertFalse(IoUtils.copyFile((File) null, (File) null));
        Assert.assertFalse(IoUtils.copyFile(copyTestFromPath, (String) null));
        Assert.assertFalse(IoUtils.copyFile(new File(copyTestFromPath), (File) null));
        Assert.assertFalse(IoUtils.copyFile(copyTestFromPath + "NoSuch", getCopyTestBackupFilePath().getAbsolutePath()));
        Assert.assertFalse(IoUtils.copyFile(new File(copyTestFromPath + "NoSuch"), getCopyTestBackupFilePath()));
        Assert.assertTrue(IoUtils.copyFile(new File(copyTestFromPath), getCopyTestBackupFilePath()));
        Assert.assertTrue(getCopyTestBackupFilePath().isFile());
        // do not overwrite
        Assert.assertFalse(IoUtils.copyFile(new File(copyTestFromPath), getCopyTestBackupFilePath()));
        // overwrite
        Assert.assertTrue(IoUtils.copyFile(new File(copyTestFromPath), getCopyTestBackupFilePath(), true));
        // overwrite again
        Assert.assertTrue(IoUtils.copyFile(new File(copyTestFromPath), getCopyTestBackupFilePath(), true));
    }

    @Test
    public void copyFileAsDirectories() {
        File dirTest = new File(copyTestFromPath).getParentFile();
        File targetTest = new File(dirTest.getParentFile(), dirTest.getName() + ".del");
        Assert.assertTrue(dirTest.isDirectory());
        Assert.assertFalse(targetTest.isDirectory());

        Assert.assertTrue(IoUtils.copyFile(dirTest, targetTest));
        Assert.assertTrue(targetTest.isDirectory());
        // do not overwrite
        Assert.assertFalse(IoUtils.copyFile(dirTest, targetTest));
        // overwrite
        Assert.assertTrue(IoUtils.copyFile(dirTest, targetTest, true));
        // overwrite again
        Assert.assertTrue(IoUtils.copyFile(dirTest, targetTest, true));
        // clean stuff
        Assert.assertTrue(IoUtils.deleteDirectory(targetTest));
    }

    @Test
    public void createDirectories() {
        // bad input - null
        ActionResult result = IoUtils.createDirectories(null);
        Assert.assertFalse(result.isSuccess());
        Assert.assertEquals("app.invalid.argument", result.getErrorCode());
        // bad input - file
        Assert.assertTrue(new File(copyTestFromPath).isFile());
        result = IoUtils.createDirectories(new File(copyTestFromPath));
        Assert.assertFalse(result.isSuccess());
        Assert.assertEquals("app.invalid.argument", result.getErrorCode());
        // New dir
        File newDir = new File(new File(copyTestFromPath).getParentFile(), Long.toString(new Date().getTime()));
        result = IoUtils.createDirectories(newDir);
        Assert.assertTrue(result.isSuccess());
        Assert.assertTrue(newDir.isDirectory());
        // again
        result = IoUtils.createDirectories(newDir);
        Assert.assertTrue(result.isSuccess());
        // attempt to create when parent dir is missing
        Assert.assertTrue(IoUtils.deleteDirectory(newDir));
        File newNestedDir = new File(newDir, Long.toString(new Date().getTime()));
        Assert.assertFalse(newNestedDir.getParentFile().exists());
        result = IoUtils.createDirectories(newNestedDir);
        Assert.assertTrue(result.isSuccess());
        Assert.assertTrue(newNestedDir.isDirectory());
        // clean
        Assert.assertTrue(IoUtils.deleteDirectory(newNestedDir));
    }

    @Test
    public void createDirectory() {
        // bad input - null
        ActionResult result = IoUtils.createDirectory(null);
        Assert.assertFalse(result.isSuccess());
        Assert.assertEquals("app.invalid.argument", result.getErrorCode());
        // bad input - file
        Assert.assertTrue(new File(copyTestFromPath).isFile());
        result = IoUtils.createDirectory(new File(copyTestFromPath));
        Assert.assertFalse(result.isSuccess());
        Assert.assertEquals("app.invalid.argument", result.getErrorCode());
        // New dir
        File newDir = new File(new File(copyTestFromPath).getParentFile(), Long.toString(new Date().getTime()));
        result = IoUtils.createDirectory(newDir);
        Assert.assertTrue(result.isSuccess());
        Assert.assertTrue(newDir.isDirectory());
        // again
        result = IoUtils.createDirectory(newDir);
        Assert.assertTrue(result.isSuccess());
        // attempt to create when parent dir is missing
        Assert.assertTrue(IoUtils.deleteDirectory(newDir));
        File newNestedDir = new File(newDir, Long.toString(new Date().getTime()));
        Assert.assertFalse(newNestedDir.getParentFile().exists());
        result = IoUtils.createDirectory(newNestedDir);
        Assert.assertFalse(result.isSuccess());
        Assert.assertEquals("app.exception", result.getErrorCode());
        Assert.assertEquals("java.nio.file.NoSuchFileException:" + newNestedDir.getPath(), result.getErrorMessage());
        Assert.assertFalse(newNestedDir.isDirectory());
    }

    @Test
    public void unzipAll() {
        File destDir = new File("ioCopyTest/noSuch/noSuchAgain");
        unzipAllFromFileToFile(new File("src/test/resources/com/ioZipTest/test_config.zip"), destDir, "Unable to create the non-existing directory " + destDir.getAbsolutePath());
        destDir = new File("src/test/resources/com/ioZipTest/test_config.zip");
        unzipAllFromFileToFile(new File("src/test/resources/com/ioZipTest/test_config.zip"), destDir, "Not a directory " + destDir.getAbsolutePath());
        destDir = new File("ioCopyTest");
        Assert.assertFalse(destDir.exists());
        unzipAllFromFileToFile(new File("src/test/resources/com/ioZipTest/test_config.zip"), destDir, null);
        assertUnzippedContent(destDir);
        Assert.assertTrue(IoUtils.deleteDirectoryContent(destDir));
        unzipAllFromFileToFile(new File("src/test/resources/com/ioZipTest/test_config.zip"), destDir, null);
        assertUnzippedContent(destDir);
        Assert.assertTrue(IoUtils.deleteDirectoryContent(destDir));
        Assert.assertTrue(IoUtils.saveToFile("abc".getBytes(), new File(destDir, "a.txt")));
        unzipAllFromFileToFile(new File("src/test/resources/com/ioZipTest/test_config.zip"), destDir, "The destination directory \"" + destDir.getAbsolutePath() + "\" is not empty");
        Assert.assertTrue(IoUtils.deleteDirectory(destDir));
    }

    private static void unzipAllFromFileToFile(File source, File dest, String expectedException) {
        ZipInputStream zis = null;
        try {
            zis = getZipInputStream(source);
            IoUtils.unzipAll(zis, dest);
            if (expectedException != null) {
                Assert.fail("Exception expected");
            }
        } catch (IOException e) {
            if (!e.getMessage().equals(expectedException)) {
                e.printStackTrace();
                Assert.fail(e.getMessage());
            }
        } finally {
            IoUtils.close(zis);
        }
    }

    private static void assertUnzippedContent(File fromDir) {
        assertFileExistsAndContent(true, new File(fromDir, "core-site.xml"), "<name>fs.defaultFS</name>");
        assertFileExistsAndContent(true, new File(fromDir, "hadoop-env.sh"), "export JAVA_HOME");
        assertFileExistsAndContent(true, new File(fromDir, "hdfs-site.xml"), "<name>dfs.namenode.name.dir</name>");
        assertFileExistsAndContent(true, new File(fromDir, "mapred-site.xml"), "<name>mapreduce.framework.name</name>");
        assertFileExistsAndContent(true, new File(fromDir, "slaves"), "slaveFour");
        assertFileExistsAndContent(true, new File(fromDir, "yarn-site.xml"), "<name>yarn.nodemanager.aux-services</name>");
        assertFileExistsAndContent(false, new File(fromDir, "test_dir"), null);
        fromDir = new File(fromDir, "test_dir");
        assertFileExistsAndContent(true, new File(fromDir, "readme1.txt"), "readme1");
        assertFileExistsAndContent(true, new File(fromDir, "readme2.txt"), "readme2");
        assertFileExistsAndContent(false, new File(fromDir, "nested_test_dir"), null);
        fromDir = new File(fromDir, "nested_test_dir");
        assertFileExistsAndContent(true, new File(fromDir, "readme1.txt"), "readme1");
        assertFileExistsAndContent(true, new File(fromDir, "readme2.txt"), "readme2");
    }

    private static void assertFileExistsAndContent(boolean isFile, File file, String fileContentSnippet) {
        if (isFile) {
            Assert.assertTrue("No such file : " + file.getAbsolutePath(), file.isFile());
        } else {
            Assert.assertTrue("No such directory : " + file.getAbsolutePath(), file.isDirectory());
        }
        if (isFile) {
            byte[] fileBytes = readAllBytes(file);
            Assert.assertNotNull(fileBytes);
            Assert.assertTrue(new String(fileBytes).contains(fileContentSnippet));
        }
    }

    private static File getCopyTestBackupFilePath() {
        return new File(copyTestFromPath + ".bak");
    }

    private static void deleteFileIfExistsFailOnError(File toDelete, boolean isFailOnError) {
        if (!deleteFileIfExists(toDelete, isFailOnError)) {
            Assert.fail("Unable to delete file : " + toDelete);
        }
    }

    private static byte[] readAllBytes(File file) {
        if (file == null || !file.isFile()) {
            return null;
        }
        byte[] fileArray = null;
        try {
            fileArray = Files.readAllBytes(Paths.get(file.getAbsolutePath()));
        } catch (IOException e) {
            Assert.fail("Unable to read data from " + file.getAbsolutePath() + " due to :" + e.getMessage());
        }
        return fileArray;
    }

    private static boolean deleteFileIfExists(File toDelete, boolean isFailOnError) {
        if (toDelete.isFile() && !toDelete.delete()) {
            if (isFailOnError) {
                return false;
            }
            toDelete.deleteOnExit();
        }
        return true;
    }

    private static ZipInputStream getZipInputStream(File zip) throws IOException {
        if (!zip.isFile()) {
            throw new FileNotFoundException("No such file " + zip.getAbsolutePath());
        }
        return new ZipInputStream(new FileInputStream(zip));
    }
}
