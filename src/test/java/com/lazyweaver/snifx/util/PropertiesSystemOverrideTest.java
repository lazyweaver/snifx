package com.lazyweaver.snifx.util;

import java.io.IOException;
import java.util.Properties;

import org.junit.Assert;
import org.junit.Test;

public class PropertiesSystemOverrideTest {

    @Test
    public void getAndSetProperties() {
        Properties props = null;
        try {
            props = IoUtils.load("/com/test.properties", false);
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        // Normal usage
        Assert.assertNotNull(props);
        Assert.assertNotNull(props.getProperty("key1"));
        Assert.assertNotNull(props.getProperty("key2"));
        Assert.assertEquals("key1value", props.getProperty("key1"));
        Assert.assertEquals("key2value", props.getProperty("key2"));
        Assert.assertEquals("defaultVal", props.getProperty("noSuchKey", "defaultVal"));
        // Overridden usage
        System.setProperty("noSuchKey", "systemValue");
        Assert.assertEquals("systemValue", props.getProperty("noSuchKey"));
        Assert.assertEquals("systemValue", props.getProperty("noSuchKey", "defaultVal"));
        props.setProperty("noSuchKey", "anotherNewValue");//should be ignored
        Assert.assertEquals("systemValue", props.getProperty("noSuchKey"));
        Assert.assertEquals("systemValue", props.getProperty("noSuchKey", "defaultVal"));
        // Load again
        try {
            props = IoUtils.load("/com/test.properties", false);
        } catch (IOException e) {
            e.printStackTrace();
            Assert.fail(e.getMessage());
        }
        Assert.assertEquals("systemValue", props.getProperty("noSuchKey"));
        Assert.assertEquals("systemValue", props.getProperty("noSuchKey", "defaultVal"));
    }
}
